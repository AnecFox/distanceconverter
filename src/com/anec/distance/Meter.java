package com.anec.distance;

public class Meter {

    private final static int METER_TO_CENTIMETERS = 100;

    public static double metersToCentimeters(double value) {
        return value * METER_TO_CENTIMETERS;
    }

    public static double centimetersToMeters(double value) {
        return value / METER_TO_CENTIMETERS;
    }
}
