package com.anec.distance;

public class Decimeter {

    private final static int DECIMETER_TO_CENTIMETERS = 10;

    public static double decimetersToCentimeters(double value) {
        return value * DECIMETER_TO_CENTIMETERS;
    }

    public static double centimetersToDecimeters(double value) {
        return value / DECIMETER_TO_CENTIMETERS;
    }
}
