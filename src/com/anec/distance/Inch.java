package com.anec.distance;

public class Inch {

    private final static double INCH_TO_CENTIMETERS = 2.54;

    public static double inchesToCentimeters(double value) {
        return value * INCH_TO_CENTIMETERS;
    }

    public static double centimetersToInches(double value) {
        return value / INCH_TO_CENTIMETERS;
    }
}
